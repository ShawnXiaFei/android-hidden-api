# Android Hidden APIs

**Android Hidden APIs** 是Google出于API稳定性考虑而隐藏的一些类、方法和资源等特性的集合。
这些特性之所以被隐藏，是因为他们可能在下一个API版本中被修改甚至删除。

Internal APIs 在 `framework.jar` 文件的 `com.android.internal` 包中；而 Hidden APIs 在 `android.jar` 中，并且使用了 `@hide` 的Java文档注释。
现在你知道它们的区别了，但是我将把它们统称为 Hidden APIs。

这个仓库包含了自定义的 `android.jar`（注：在x分支jars目录下），你可以用来开发你的APP。
不过，若你迫切需要创建自己的 `android.jar`，请参考 Krabby Patty 的方法：[Create Your Own Android Hidden APIs](https://medium.com/@hardiannicko/create-your-own-android-hidden-apis-fa3cca02d345)。

## Use custom `android.jar`

1. 下载自定义 `android.jar`，地址： [Google Drive](https://drive.google.com/drive/folders/17oMwQ0xBcSGn159mgbqxcXXEcneUmnph)。
2. 进入 `<SDK location>/platforms/` 目录。
3. 使用下载的 `android.jar` 替换掉原始的 jar 包（注：先关闭AS等工具），比如 `android-30/android.jar`。
4. 修改 `compileSdkVersion` 和 `targetSdkVersion` 到 30 (接上例).
5. 最后，重新编译你的项目。

注意：`compileSdkVersion` 和 `targetSdkVersion` 越高越好。

## Resources Helper
![Maven Central](https://img.shields.io/maven-central/v/com.anggrayudi/android-hidden-api.svg)

如果你只想使用 Android 内部的资源，不需内部类或方法，可以这么做：

````gradle
dependencies {
    implementation 'com.anggrayudi:android-hidden-api:30.0'
}
````

这是访问内部资源的例子：

```java
String accept = InternalAccessor.getString("accept");
float sbar_height = InternalAccessor.getDimension("status_bar_height");
int notif_color = InternalAccessor.getColor("config_defaultNotificationColor");
```

## Contributing

如果你想将你自定义的 `android.jar` 添加到 [Google Drive](https://drive.google.com/drive/folders/17oMwQ0xBcSGn159mgbqxcXXEcneUmnph)，请创建一个 issue，我（注：Anggrayudi）会上传它。

## License

    Copyright 2015-2020 Anggrayudi Hardiannicko A.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


[1]: https://devmaze.wordpress.com/2011/01/18/using-com-android-internal-part-1-introduction
[2]: https://github.com/anggrayudi/android-hidden-api/issues/9
